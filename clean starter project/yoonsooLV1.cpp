/******************************************************************************/
/*!
    *This level contains basic features of this engine.
    How to control the main camera, how to set, control objects
    and basic procedure of this engine(init - update - close).

    *Instuctions
    1. Camera panning - Arrow keys
    2. Camera zooming - W(Zooming out), S(Zooming in)
    3. Camera rotating - Q(Counter-Clockwise), E(Clockwise)
    4. Set animation on specific frame - Number keys (1-6)
    5. Stop the animation- Space
    6. Rewind the animation - Back space
    7. Play the animation from start - ENTER
    8. Move to next state - C
*/
/******************************************************************************/

#include "CommonLevel.h"
#include "yoonsooLV1.h"

const  SDL_Color SKY_BLUE = { 64, 64, 255, 255 };
float currentPlayerLoc;

void Level1::Initialize()
{
    m_backgroundColor = SKY_BLUE;

   /* m_backgroundColor.r = 255, m_backgroundColor.g = 64,
        m_backgroundColor.b = 0;*/
    // Set Camera
    // You don't need to do this anymore
    //camera.Initialize(int(State::m_width), int(State::m_height));
    // Set position of the camera
    camera.position.Set(0, 0, 0);

    // Set main font
    // Load the font file to use as a main font
    // and set the default size of it
    mainFont = TTF_OpenFont("font/Default.ttf", 48);

    //Set Pong
    //Pong.SetName("PONG");
    //Pong.transform.position.Set(0.f, 0.f, 0.f);
    //Pong.transform.SetScale(128, 128);
    ////Pong.sprite.color = SDL_Color{0,255,255,255};
    //Pong.transform.rotation = 0;
    //Pong.sprite.LoadImage("texture/haha.png", m_renderer);

    //현재 진행되고 있는 레벨에서 모든물체에 중력을 적용 -> 즉 static이 아닌이상 밑으로 떨어질것임

    b2Vec2 gravity(0.f, -9.8f);
    SetPhysicsWorld(gravity);

    animation.SetName("Walking boy");
    animation.transform.position.Set(-400.f, 0.f, 0.f);
    animation.transform.SetScale(90, 150);
    //animation.transform.rotation = 45.F;
    animation.sprite.LoadImage("texture/animation.png", State::m_renderer);
    // Set toggle true so animation activates 
    animation.sprite.activeAnimation = true;
    // Set the number of frames to update
    animation.sprite.SetFrame(8);
    // Set the speed of frame updating
    animation.sprite.SetSpeed(20.f);
    //카메라 object에 계속 맞춰서 가는거
   // animation.sprite.isHud = true;


    animation.physics.bodyType = Physics::DYNAMIC;
    // Set body shape
    // Set density
    animation.physics.SetDensity(10.F);
    // Set restitution -> 튕겨 나가는거
    //animation.physics.SetRestitution(.75f);
    // Generate rigidbody passing world and transform

    animation.physics.ActiveGhostCollision(true);
    animation.physics.GenerateBody(GetPhysicsWorld(), &animation.transform);

	InitializeMapGenerator();

    AddObject(&animation);
    // AddObject(&newTilePoint);
}

void Level1::Update(float dt)
{
   // TransformObjects(dt);
    ControlCamera(dt);
   // SetAnimation();
	UpdateMapGenerator();

    // Move to Level2 by pressing 'C' key
   /* if (m_input->IsTriggered(SDL_SCANCODE_C))
        m_game->Change(LV_Level2);
*/
    // Must be one of the last functions called at the end of State Update
    UpdatePhysics(dt);
    Render(dt);
}

void Level1::Close()
{
    // Wrap up state
    ClearBaseState();
}

void Level1::InitializeMapGenerator(void)
{
	Map m1(GetPhysicsWorld());
	m1.SetObject(b2Vec2{ -300.f, -400.f });
	mapList.PushFront(m1);
	mapList.GetTailValue().GetObject().physics.GenerateBody(GetPhysicsWorld(), &mapList.GetTailValue().GetObject().transform);

	AddObject(mapList.GetTailValue().GetObjectAddress());
}

void Level1::UpdateMapGenerator(void)
{
	// If map list count is equal or less than 1
	if (mapList.Count()<= 1)
	{
		Map tempMap(GetPhysicsWorld());
		tempMap.SetObject(b2Vec2{ animation.transform.position.x + 200.f, -400.f });
		mapList.PushBack(tempMap);
		mapList.GetTailValue().GetObject().physics.GenerateBody(GetPhysicsWorld(), &mapList.GetTailValue().GetObject().transform);

		AddObject(mapList.GetTailValue().GetObjectAddress());
	}

	// map list node go left side from character's position
	if (camera.position.x - 600.f > mapList.GetHeadValue().GetObject().transform.position.x)
	{
		RemoveObject(mapList.GetHeadValue().GetObjectAddress());
		mapList.DeleteHead();
	}
}


void Level1::ControlCamera(float dt)
{
    // Set the camera's speed
    static float speed = 120.f * dt;
    static bool toggle = true;


    
    camera.position.x += speed;
    animation.transform.position.x += speed;
    // Panning by pressing arrow keys
    if (m_input->IsPressed(SDL_SCANCODE_RIGHT))
        animation.transform.position.x += speed;
    if (m_input->IsPressed(SDL_SCANCODE_LEFT))
        animation.transform.position.x -= speed;

    if (m_input->IsTriggered(SDL_SCANCODE_P)) {
        newTilePoint.physics.ActiveGhostCollision(toggle);
        toggle = !toggle;
    }

    if(m_input->IsTriggered(SDL_SCANCODE_K))//animation.transform.position.x >= currentPlayerLoc +200)
    {
        Object *newTile = new Object;

        newTile->SetName("new");

        newTile->transform.position.Set(animation.transform.position.x+10, -150.f, 0.f);
        newTile->transform.SetScale(400, 32);
        newTile->physics.bodyType = Physics::STATIC;
       // newTile->sprite.color = Level5Color::BLACK;
        newTile->sprite.LoadImage("texture/rect.png", m_renderer);

        newTile->physics.GenerateBody(GetPhysicsWorld(), &newTile->transform);


        AddObject(newTile);

        //float scaleX = TURRET_WIDTH;
        //float scaleY = TURRET_HEIGHT;
        //newTurret->transform.SetScale(scaleX, scaleY);

        //newTurret->sprite.color = Level5Color::BLACK;
        //newTurret->sprite.LoadImage("texture/rect.png", m_renderer);

        //AddObject(newTurret);
        
    }

    //if (m_input->IsPressed(SDL_SCANCODE_DOWN))
    //    camera.position.y += speed;
    //if (m_input->IsPressed(SDL_SCANCODE_UP))
    //    camera.position.y -= speed;

    ////Zooming by pressing arrow W and S key
    //if (m_input->IsPressed(SDL_SCANCODE_W))
    //    camera.position.z -= speed;
    //if (m_input->IsPressed(SDL_SCANCODE_S))
    //    camera.position.z += speed;

    ////Rotating by pressing arrow Q and E key
    //if (m_input->IsPressed(SDL_SCANCODE_Q))
    //    camera.rotation -= speed;
    //if (m_input->IsPressed(SDL_SCANCODE_E))
    //    camera.rotation += speed;
}

