/******************************************************************************/
/*!
\file   Level1.h
\author David Ly and Juyong Jeong
\par    email: dly\@digipen.edu
\par    GAM150 demo
\par	v0
\date   2018/03/11

Containing examples how to use engine
*/
/******************************************************************************/

#pragma once
#include "engine\State.h"
#include "engine\Object.h"
#include "List.h"

class Level1 : public State
{
    friend class Game;

protected:

    Level1() : State() {}; 
        ~Level1() override {};

    // Derived initialize function
    void Initialize() override;
    // Derived Update function
    void Update(float dt) override;
    // Derived Close function
    void Close() override;

	void InitializeMapGenerator(void);
	void UpdateMapGenerator(void);

private:

  //  void SetIntroTexts();
  //  void SetAnimation();
    void ControlCamera(float dt);
   // void TransformObjects(float dt);

    const int period = 180;
    float ellapsedtime = 0.0f;

    // Objects
   /* Object mario, animation,
        background, letters,Pong;*/
    Object Pong,animation,wall, newTilePoint;

    Object titleText2;

    Object instructionsText2;
    Object	introTexts[6];

	List mapList;

	Object block;
};
