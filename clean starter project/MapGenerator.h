#pragma once
#include <engine/Object.h>
#include <engine/State.h>

// Big problem is it works only one Object.
// I need to change it works even if there are a lot of objects.

class Map
{
public:
	// default constructor
	Map();

	// It is really bad code...
	// Need to be changed.
	Map(b2World* world);

	// Reference Copy Constructor
	Map(Map &value);

	// Set block Object. It works only one object.
	void SetObject(b2Vec2 point);

	Object* GetObjectAddress(void);
	Object& GetObject(void);

private:
	Object block;
	b2World* physicsWorld;
};