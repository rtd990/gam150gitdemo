#include "MapGenerator.h"

Map::Map()
{
}

Map::Map(b2World * world)
	:physicsWorld(world)
{
}

Map::Map(Map & value)
{
	// reference constructor
	block.SetName(value.block.GetName().c_str());
	block.transform.position = value.block.transform.position;
	block.transform.SetScale(value.block.transform.GetScale());
	block.transform.rotation = value.block.transform.rotation;

	block.sprite.LoadImage("texture/rect.png", State::m_renderer);

	block.physics.bodyType = Physics::STATIC;

	block.transform.rotation = 0; 
	block.sprite.color = SDL_Color{ 0, 255, 255, 255 };
	block.sprite.LoadImage("texture/rect.png", State::m_renderer);
	block.sprite.color.r = block.sprite.color.g
		= block.sprite.color.b = 0X88;
	block.physics.bodyType = Physics::STATIC;
	//block.physics.GenerateBody(physicsWorld, &block.transform);
}

void Map::SetObject(b2Vec2 point)
{
	// Set Object data
	block.SetName("block");

	block.transform.position.Set(point.x, point.y, 0.f);
	block.transform.SetScale(400, 320);
	block.transform.rotation = 0;

	block.sprite.LoadImage("texture/rect.png", State::m_renderer);

	block.physics.bodyType = Physics::STATIC;

}

Object* Map::GetObjectAddress(void)
{
	return &block;
}


Object& Map::GetObject(void)
{
	return block;
}
