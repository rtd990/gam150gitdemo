#pragma once

#include "engine/State.h"
#include "engine/Object.h"

class LevelSinil: public State
{
	friend class Game;

protected:
	LevelSinil() : State() {};
	~LevelSinil() override {};

	void Initialize() override;
	void Update(float dt) override;
	void Close() override;

private:
	Object houseObject;
};